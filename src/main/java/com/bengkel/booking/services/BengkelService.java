package com.bengkel.booking.services;

import com.bengkel.booking.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BengkelService {
	
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
    public static Customer loginCustomer(List<Customer> listAllCustomer, Scanner input){
        int gagalLogin = 0;
        Customer customer;
        boolean isLoop = true;

        System.out.println();
        System.out.println("Login Customer");
        System.out.println("+---------------------------------+");
        do {
            System.out.println("Masukan ID Customer : ");
            String customerId = input.nextLine();

            customer = listAllCustomer.stream()
                    .filter(data-> data.getCustomerId().equalsIgnoreCase(customerId))
                    .findFirst()
                    .orElse(null);

            if (customer == null){
                System.out.println("ID Custumer Yang Dimasukan Tidak Ada Yang Cocok");
                gagalLogin++;
                Validation.validasiGagalLogin(gagalLogin);
            }else {
                do {
                    System.out.println("Masukan Password Customer : ");
                    String customerPassword = input.nextLine();

                    if (customer.getPassword().equals(customerPassword)){
                        System.out.println("Login Customer Berhasil");
                        System.out.println();
                        isLoop = false;
                    }else {
                        System.out.println("Password Tidak Cocok");
                        gagalLogin++;
                        Validation.validasiGagalLogin(gagalLogin);
                    }
                }while(isLoop);
            }
        }while (isLoop);
        return customer;
    }
	
	//Info Customer
    public static void infoCustomer(Customer customer){
        System.out.println();
        System.out.println("Informasi Customer");
        System.out.println("+---------------------------------+");

        String statusMember="Non Member";
        if (customer instanceof MemberCustomer){
            statusMember = "Member";
        }
        System.out.println(statusMember);

        System.out.printf("%20s", "Customer Profile");
        System.out.printf("\n%20s %-30s", "Customer ID : ", customer.getCustomerId());
        System.out.printf("\n%20s %-30s", "Nama : ", customer.getName());
        System.out.printf("\n%20s %-30s", "Customer Status : ", statusMember);
        System.out.printf("\n%20s %-30s", "Alamat : ", customer.getAddress());
        if (customer instanceof MemberCustomer){
            System.out.printf("\n%20s %-30s", "Saldo : ", PrintService.printDecimal(((MemberCustomer) customer).getSaldoCoin()));
        }
        System.out.println("\nList Kendaraan : ");
        PrintService.printVechicle(customer.getVehicles());
    }
	
	//Booking atau Reservation

    public static void bookingService(Customer customer, List<ItemService> listAllService, List<BookingOrder> bookingService, Scanner input){
        List<ItemService> tempListService = new ArrayList<>();
        BookingOrder bookingOrder = new BookingOrder();
        System.out.println();
        System.out.println("Booking Bengkel");
        System.out.println("+---------------------------------+");

        Vehicle pilihVehicle;
        boolean isLoop = true;
        do {
            System.out.println("Masukan Vehicle Id : ");
            String idKendaraan = input.nextLine();

            List<Vehicle> listVehicleCustomer = customer.getVehicles();

            pilihVehicle = listVehicleCustomer.stream()
                    .filter(vehicle-> vehicle.getVehiclesId().equalsIgnoreCase(idKendaraan))
                    .findFirst()
                    .orElse(null);

            if (pilihVehicle == null){
                System.out.println("ID Kendaraan Yang Dimasukan Tidak Ada Yang Cocok");
            }else {
                System.out.println();
                System.out.println("List Service yang Tersedia : ");

                PrintService.printListService(pilihVehicle, listAllService);
                List<ItemService> pilihService = tambahService(customer, listAllService, tempListService, input);
                String paymentMetode = Validation.paymentMetode(customer);

                double totalPaymentService = pilihService.stream()
                        .mapToDouble(data-> data.getPrice())
                        .sum();

                if (paymentMetode.equalsIgnoreCase("Saldo Coin")){
                    double saldoCustomer = (((MemberCustomer)customer).getSaldoCoin());

                    if (saldoCustomer<totalPaymentService){
                        System.out.println("Booking gagal karena saldo tidak cukup. Harus top up kembali untuk mendapatkan diskon service");
                        System.out.println();
                        System.out.printf("%-20s %30s", "Sisa Saldo : ", PrintService.printDecimal(saldoCustomer) +"\n");
                        System.out.printf("%-20s %30s", "Total Harga Service : ", PrintService.printDecimal(totalPaymentService));
                        isLoop = false;
                    }else{
                        bookingOrder.setCustomer(customer);
                        bookingOrder.setServices(pilihService);
                        bookingOrder.setPaymentMethod(paymentMetode);
                        bookingOrder.setTotalServicePrice(totalPaymentService);
                        bookingOrder.generateStringId();
                        bookingOrder.calculatePayment();

                        bookingService.add(bookingOrder);

                        double totalPaymentWithDiskon= bookingOrder.getTotalPayment();
                        double sisaSaldoCustomer = saldoCustomer - totalPaymentWithDiskon;

                        ((MemberCustomer) customer).setSaldoCoin(sisaSaldoCustomer);

                        paymentService(bookingOrder);
                        isLoop = false;
                    }
                }else {
                    bookingOrder.setCustomer(customer);
                    bookingOrder.setServices(pilihService);
                    bookingOrder.setPaymentMethod(paymentMetode);
                    bookingOrder.setTotalServicePrice(totalPaymentService);
                    bookingOrder.generateStringId();
                    bookingOrder.calculatePayment();

                    bookingService.add(bookingOrder);
                    paymentService(bookingOrder);
                    isLoop = false;
                }
            }
        }while (isLoop);
    }

    public static List<ItemService> tambahService(Customer customer, List<ItemService> listAllService, List<ItemService> tempListService, Scanner input){
        boolean isLoop = true;
        int totalListService = 0;

        if (customer instanceof MemberCustomer){
            customer.setMaxNumberOfService(2);
        }

        do {
            System.out.println("Masukan Service Id : ");
            String idService = input.nextLine();

            ItemService inputService = listAllService.stream()
                    .filter(service-> service.getServiceId().equalsIgnoreCase(idService))
                    .findFirst()
                    .orElse(null);

            if (inputService == null){
                System.out.println("ID Service Yang Dimasukan Tidak Ada Yang Cocok");
            }else {
                if (!(tempListService.contains(inputService))){
                    tempListService.add(inputService);
                    totalListService++;
                    if (totalListService < customer.getMaxNumberOfService()){
                        isLoop = Validation.validasiTambahService();
                    }else {
                        isLoop = false;
                    }
                }else {
                    System.out.println("Layanan Service Sudah Terdaftar, Pilih Layanan Yang Lain");
                }
            }
        }while (isLoop);
        return tempListService;
    }

    public static void paymentService(BookingOrder bookingOrder){
        System.out.println();
        System.out.println("Booking Berhasil ");
        System.out.printf("%-20s %30s", "Total Harga Service : ", PrintService.printDecimal(bookingOrder.getTotalServicePrice()) +"\n");
        System.out.printf("%-20s %30s", "Total Pembayaran : ", PrintService.printDecimal(bookingOrder.getTotalPayment()) +"\n");
        System.out.println();
    }
	
	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpSaldo(Customer customer){
        System.out.println();
        System.out.println("Top Up Saldo");
        System.out.println("+---------------------------------+");
        if (customer instanceof MemberCustomer){
            int totalTopUpSaldo = Validation.validasiNumberWithRange(
                    "Masukan besaran Top Up: ",
                    "Tidak Valid",
                    "^[0-9]+$",
                    999_999_999,
                    100
            );

            double saldoCustomer = ((MemberCustomer) customer).getSaldoCoin();

            ((MemberCustomer) customer).setSaldoCoin(saldoCustomer + totalTopUpSaldo);
        }else {
            System.out.println("Mohon Maaf, Fitur ini hanya untuk member bengkel");
        }
    }

    public static void printListBooking(List<BookingOrder> listAllBookingOrder) {
        PrintService.printListBooking(listAllBookingOrder);
    }
	//Logout
	
}
