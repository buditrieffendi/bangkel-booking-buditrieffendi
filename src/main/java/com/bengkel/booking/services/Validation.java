package com.bengkel.booking.services;

import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.MemberCustomer;

import java.util.Scanner;

public class Validation {
	
	public static String validasiInput(String question, String errorMessage, String regex) {
	    Scanner input = new Scanner(System.in);
	    String result;
	    boolean isLooping = true;
	    do {
	      System.out.print(question);
	      result = input.nextLine();

	      //validasi menggunakan matches
	      if (result.matches(regex)) {
	        isLooping = false;
	      }else {
	        System.out.println(errorMessage);
	      }

	    } while (isLooping);

	    return result;
	}
	
	public static int validasiNumberWithRange(String question, String errorMessage, String regex, int max, int min) {
	    int result;
	    boolean isLooping = true;
	    do {
	      result = Integer.valueOf(validasiInput(question, errorMessage, regex));
	      if (result >= min && result <= max) {
	        isLooping = false;
	      }else {
	        System.out.println("Pilihan angka " + min + " s.d " + max);
	      }
	    } while (isLooping);

	    return result;
	}

	public static void validasiGagalLogin(int gagalLogin){
		if (gagalLogin == 3){
			System.out.println("Sudah gagal 3x untuk login. Aplikasi dipaksa untuk berhenti");
			System.exit(0);
		}
	}

	public static boolean validasiTambahService(){
		boolean isLooping = false;
		boolean isLanjut = false;
		do {
			String result = validasiInput(
					"Apakah anda ingin menambahkan Service Lainnya? (Y/T) : ",
					"Input Tidak Valid",
					"^[a-zA-Z]+$").toUpperCase();

			switch (result){
				case "Y":
					isLanjut = true;
					break;
				case "T":
					isLanjut = false;
					break;
				default:
					System.out.println("pilih Y untuk lanjut atau T untuk berhenti tambah list service");
					isLooping = true;
			}
		}while (isLooping);
        return isLanjut;
    }

	public static String paymentMetode(Customer customer){
		String metode = "Cash";
		if (customer instanceof MemberCustomer){
			boolean isLoop = false;
			do {
				System.out.println();
				metode = Validation.validasiInput(
						"Silahkan Pilih Metode Pembayaran (Saldo Coin atau Cash) : ",
						"Tidak Valid",
						"^[a-zA-Z ]+$");
				switch (metode){
					case "Saldo Coin":
						isLoop = true;
						break;
					case "Cash":
						isLoop = true;
						break;
					default:
						System.out.println("Metode pembayaran tidak valid");
				}
			}while(!isLoop);
		}else {
			System.out.println();
			System.out.println("Metode Pembayaran Menggunakan : "+ metode);
		}
		return metode;
	}
}
