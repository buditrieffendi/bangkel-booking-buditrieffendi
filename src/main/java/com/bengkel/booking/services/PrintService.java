package com.bengkel.booking.services;

import java.text.DecimalFormat;
import java.util.List;

import com.bengkel.booking.models.*;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-5s |%n";
		String line = "+----+-----------------+------------+-----------------+--------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Tipe Kendaraan", "Tahun");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicleType, vehicle.getYearRelease());
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.

	public static void printListService(Vehicle pilihVehicle, List<ItemService> listAllItemService) {
		String formatTable = "| %-2s | %15s | %-26s | %-15s | %15s |%n";
		String line = "+----+-----------------+----------------------------+-----------------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
		System.out.format(line);
		int number = 1;

		for (ItemService data : listAllItemService) {
			if (data.getVehicleType().equalsIgnoreCase(pilihVehicle.getVehicleType())){
				System.out.format(formatTable, number, data.getServiceId(), data.getServiceName(), data.getVehicleType(), PrintService.printDecimal(data.getPrice()));
			}
			number++;
		}


		System.out.printf(line);
	}

	public static void printListBooking(List<BookingOrder> listAllBookingOrder) {
		

		String formatTable = "| %-2s | %-20s | %-20s | %-15s | %20s | %20s | %-40s |%n";
		String line = "+----+----------------------+----------------------+-----------------+----------------------+----------------------+------------------------------------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Payment Metode", "Total Service", "Total Payment", "List Service");
		System.out.format(line);
		int number = 1;

		for (BookingOrder data : listAllBookingOrder) {
			System.out.format(formatTable, number, data.getBookingId(), data.getCustomer().getCustomerId(), data.getPaymentMethod(), PrintService.printDecimal(data.getTotalServicePrice()), PrintService.printDecimal(data.getTotalPayment()), getListService(data.getServices()));
			number++;
		}

		System.out.printf(line);
	}
	
	public static String getListService(List<ItemService> listService){
		String listBooking = "";
		for(ItemService data: listService){
			listBooking += data.getServiceName()+", ";
		}
        return listBooking;
    }

	public static String printDecimal(double angka){
		DecimalFormat decimalFormat = new DecimalFormat("###,###,##0.00");

        return decimalFormat.format(angka);
	}
}
