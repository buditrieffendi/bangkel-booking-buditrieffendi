package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<BookingOrder> bookingOrder = new ArrayList<>();
	private static Scanner input = new Scanner(System.in);
	public static void run() {
		String[] listMenu = {"Login", "Exit"};
		int menuChoice = 0;
		boolean isLooping = true;

		do {
			PrintService.printMenu(listMenu, "=====  APLIKASI BOOKING BENGKEL  =====");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu: ", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);

			switch (menuChoice) {
				case 1:
					login();
					break;
				default:
					System.out.println("===  TERIMA KASIH  ===");
					System.exit(0);
					break;
			}
		} while (isLooping);

	}
	
	public static void login() {
		Customer customerLogin = BengkelService.loginCustomer(listAllCustomers, input);
		mainMenu(customerLogin);
	}
	
	public static void mainMenu(Customer customerLogin) {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu: ", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			
			switch (menuChoice) {
			case 1:
				BengkelService.infoCustomer(customerLogin);
				break;
			case 2:
				BengkelService.bookingService(customerLogin,listAllItemService, bookingOrder, input);
				break;
			case 3:
				BengkelService.topUpSaldo(customerLogin);
				break;
			case 4:
				BengkelService.printListBooking(bookingOrder);
				break;
			default:
				System.out.println("Logout");
				isLooping = false;
				break;
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
